<?php

namespace Drupal\Tests\menu_bulk_add_items\Unit;

use Drupal\menu_bulk_add_items\Form\MenuBulkAddItemsForm;
use PHPUnit\Framework\TestCase;

class TreeTest extends TestCase {

  /**
   * @covers MenuBulkAddItemsForm::convertTextTreeToNestedArray
   */
  public function testConvertTextTreeToNestedArray(): void {
    $text = <<<TEXT
      1
      2
      TEXT;
    $result = [
      0 => [
        'text' => '1',
      ],
      1 => [
        'text' => '2',
      ],
    ];
    $this->assertSame($result, MenuBulkAddItemsForm::convertTextTreeToNestedArray($text));

    $text = <<<TEXT
      1
      - 1.1
      TEXT;
    $result = [
      0 => [
        'text' => '1',
        'children' => [
          1 => [
            'text' => '1.1',
          ],
        ],
      ],
    ];
    $this->assertSame($result, MenuBulkAddItemsForm::convertTextTreeToNestedArray($text));

    $text = <<<TEXT
      1
      - 1.1
      - 1.2
      TEXT;
    $result = [
      0 => [
        'text' => '1',
        'children' => [
          1 => [
            'text' => '1.1',
          ],
          2 => [
            'text' => '1.2',
          ],
        ],
      ],
    ];
    $this->assertSame($result, MenuBulkAddItemsForm::convertTextTreeToNestedArray($text));

    $text = <<<TEXT
      1
      - 1.1
      -- 1.1.1
      TEXT;
    $result = [
      0 => [
        'text' => '1',
        'children' => [
          1 => [
            'text' => '1.1',
            'children' => [
              2 => [
                'text' => '1.1.1',
              ],
            ],
          ],
        ],
      ],
    ];
    $this->assertSame($result, MenuBulkAddItemsForm::convertTextTreeToNestedArray($text));

    $text = <<<TEXT
      1
      - 1.1
      -- 1.1.1
      - 1.2
      TEXT;
    $result = [
      0 => [
        'text' => '1',
        'children' => [
          1 => [
            'text' => '1.1',
            'children' => [
              2 => [
                'text' => '1.1.1',
              ],
            ],
          ],
          3 => [
            'text' => '1.2',
          ],
        ],
      ],
    ];
    $this->assertSame($result, MenuBulkAddItemsForm::convertTextTreeToNestedArray($text));

    $text = <<<TEXT
      1
      - 1.1
      -- 1.1.1
      2
      TEXT;
    $result = [
      0 => [
        'text' => '1',
        'children' => [
          1 => [
            'text' => '1.1',
            'children' => [
              2 => [
                'text' => '1.1.1',
              ],
            ],
          ],
        ],
      ],
      3 => [
        'text' => '2',
      ],
    ];
    $this->assertSame($result, MenuBulkAddItemsForm::convertTextTreeToNestedArray($text));

    $text = <<<TEXT
      1
      - 1.1
      -- 1.1.1
      -- 1.1.2
      2
      - 2.1
      - 2.2
      -- 2.2.1
      -- 2.2.2
      --- 2.2.2.1
      TEXT;
    $result = [
      0 => [
        'text' => '1',
        'children' => [
          1 => [
            'text' => '1.1',
            'children' => [
              2 => [
                'text' => '1.1.1',
              ],
              3 => [
                'text' => '1.1.2',
              ],
            ],
          ],
        ],
      ],
      4 => [
        'text' => '2',
        'children' => [
          5 => [
            'text' => '2.1',
          ],
          6 => [
            'text' => '2.2',
            'children' => [
              7 => [
                'text' => '2.2.1',
              ],
              8 => [
                'text' => '2.2.2',
                'children' => [
                  9 => [
                    'text' => '2.2.2.1',
                  ],
                ],
              ],
            ],
          ],
        ],
      ],
    ];
    $this->assertSame($result, MenuBulkAddItemsForm::convertTextTreeToNestedArray($text));
  }

}
