<?php

namespace Drupal\menu_bulk_add_items\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\menu_link_content\MenuLinkContentInterface;
use Drupal\system\MenuInterface;

class MenuBulkAddItemsForm extends FormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'menu_bulk_add_items_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, MenuInterface $menu = NULL): array {
    $form['items'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Items'),
      '#description' => $this->t('Example: @code', ['@code' => Markup::create('
        <pre style="white-space:pre-line;">
          Item 1 = /item-1
          - Item 1.1 = /item-1/item-1-1
          - Item 1.2 = /item-1/item-1-2
          -- Item 1.2.1 = /item-1/item-1-2/item-1-2-1
          -- Item 1.2.2 = /item-1/item-1-2/item-1-2-2
          Item 2 = /item-2
        </pre>
      ')]),
      '#rows' => 15,
    ];

    $form['initial_weight'] = [
      '#type' => 'number',
      '#title' => $this->t('Initial weight'),
      '#default_value' => 0,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add'),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $menu = $form_state->getBuildInfo()['args'][0]; /** @var MenuInterface $menu */
    $items_tree = self::convertTextTreeToNestedArray($form_state->getValue('items'));
    $this->createMenuItems($menu->id(), $items_tree, (int)$form_state->getValue('initial_weight'));
  }

  /**
   * Create menu items.
   */
  public function createMenuItems(string $menu_name, array $items_tree, int $initial_weight, MenuLinkContentInterface $parent_menu_link = NULL): void {
    foreach ($items_tree as $item) {
      $exploded_item_text = explode('=', $item['text']);
      $menu_link_title = trim($exploded_item_text[0]);
      $menu_link_url = isset($exploded_item_text[1]) ? trim($exploded_item_text[1]) : '<none>';

      $menu_link = MenuLinkContent::create([
        'title' => $menu_link_title,
        'menu_name' => $menu_name,
        'link' => self::convertUserEnteredStringToUri($menu_link_url),
        'parent' => $parent_menu_link ? 'menu_link_content:' . $parent_menu_link->uuid() : NULL,
        'weight' => $initial_weight++,
      ]);
      $menu_link->save();

      if (!empty($item['children'])) {
        $this->createMenuItems($menu_name, $item['children'], $initial_weight, $menu_link);
      }
    }
  }

  /**
   * Convert text tree to nested array.
   */
  public static function convertTextTreeToNestedArray(string $text): array {
    $lines = explode("\n", trim($text));
    $lines = array_map('trim', $lines);
    $lines = array_diff($lines, ['']);
    $nested_array = [
      // "-1" it's line number
      -1 => [
        'text' => 'Root',
      ],
    ];
    $branches = [
      // "-1" it's depth
      -1 => &$nested_array,
    ];
    $prev_line_depth = -1;

    foreach ($lines as $line_number => $line) {
      $line_depth = 0;
      if (preg_match('/^-+/', $line, $matches)) {
        $line_depth = mb_strlen($matches[0]);
      }

      if ($line_depth > $prev_line_depth) {
        // Create 'children' element in parent item
        $branches[$prev_line_depth][$line_number - 1]['children'] = [];
        // Create branch
        $branches[$line_depth] = &$branches[$prev_line_depth][$line_number - 1]['children'];
      }

      $branches[$line_depth][$line_number]['text'] = trim(mb_substr($line, $line_depth));

      $prev_line_depth = $line_depth;
    }

    return $nested_array[-1]['children'];
  }

  /**
   * Convert user entered string to uri.
   *
   * @see LinkWidget::getUserEnteredStringAsUri()
   */
  public static function convertUserEnteredStringToUri(string $string): string {
    $uri = trim($string);

    if (in_array($string, ['<nolink>', '<none>', '<button>'], TRUE)) {
      $uri = 'route:' . $string;
    }
    elseif (!empty($string) && parse_url($string, PHP_URL_SCHEME) === NULL) {
      $uri = 'internal:' . $string;
    }

    return $uri;
  }

}
